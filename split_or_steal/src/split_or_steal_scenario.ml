open Sos_ocaml_interface
open Tzfunc.Rp

module B = Blockchain

let compute_hash secret nonce =
  let open Tzfunc.Rp in
  let open Factori_types in
  let$ secret_pack = Tzfunc.Forge.pack string_micheline (string_encode secret) in
  let$ secret_nonce = Tzfunc.Forge.pack string_micheline (string_encode nonce) in
  let phrase = Crypto.coerce (secret_pack) ^ (Crypto.coerce secret_nonce) in
  Result.Ok (Digestif.SHA256.digest_bigstring @@ Bigstring.of_string @@ phrase)

let compute_string_hash secret nonce =
  let open Tzfunc.Rp in
  match (let$ hash = compute_hash secret nonce in
         Result.Ok (Digestif.SHA256.to_hex @@ hash)) with
  | Ok hash -> hash
  | _ -> failwith "error in hash computation"

(* let deploy2 ?(node = B.ithaca_node) id initial_storage =
   let>? kt1 = deploy ~node ~name:"split_or_steal" ~from:id initial_storage in
   Format.eprintf "Kt1 : %s" kt1;
   Lwt.return_ok () *)

let init_storage =
  {
    player1 = None;
    player2 = None;
    current_players = Z.of_string "0";
    choice1 = "";
    choice2 = "";
    choice1_confirm = false;
    choice2_confirm = false;
    choice1_hash = Crypto.H.mk "";
    choice2_hash = Crypto.H.mk "";
    current_state = Registration
  }

let main () =
  Format.eprintf "\n\n--------------- Beginning of Split or Steal scenario ---------------\n@.";
  let _ = Tzfunc.Node.set_silent true in
  let alice = B.alice_flextesa in
  let bob = B.bob_flextesa in
  let node = B.ithaca_node in
  (* Deploying the contract. *)
  Format.printf "----------------------------- Contract -----------------------------@.";
  Format.printf "Deploying the contract...%!";
  (* let>? sos_contract = deploy ~node ~name:"split_or_steal" ~from:alice init_storage in *)
  let sos_contract = "KT1ST6Haqh8pKVcpoLK82i4WvFXXkiJ3Ssse" in
  Format.printf "(KT1 : %s)@." sos_contract;
  (* Entering Phase. *)
  Format.printf "--------------------------- Registration ---------------------------@.";
  Format.printf "Alice & Bob entering the game...@.";
  let register_alice () = call_enterGame ~node ~amount:210L ~from:alice ~kt1:sos_contract () in
  let register_bob () = call_enterGame ~node ~amount:210L ~from:bob ~kt1:sos_contract () in
  let>? _ = B.parallel_calls (Format.printf "Operation Hash : %s@.") [register_alice; register_bob] in
  (* Playing Phase. *)
  Format.printf "------------------------------ Playing -----------------------------@.";
  Format.printf "Alice & Bob are playing...@.";
  let alice_choice, alice_secret = "Steal", "3N1C4Y" in
  let alice_choice_hash = compute_string_hash alice_choice alice_secret in
  let alice_choice_bytes = Crypto.H.mk alice_choice_hash in
  Format.printf "Alice playing with the hashed answer :@.%s@." alice_choice_hash;
  let play_alice () = call_play ~node ~from:alice ~kt1:sos_contract alice_choice_bytes in
  let bob_choice, bob_secret = "Split", "04004873" in
  let bob_choice_hash = compute_string_hash bob_choice bob_secret in
  let bob_choice_bytes = Crypto.H.mk bob_choice_hash in
  Format.printf "Bob playing with the hashed answer :@.%s@." bob_choice_hash;
  let play_bob () = call_play ~node ~from:bob ~kt1:sos_contract bob_choice_bytes in
  let>? _ = B.parallel_calls (Format.printf "Operation Hash : %s@.") [play_alice; play_bob] in
  (* Revealing Phase. *)
  Format.printf "----------------------------- Revealing ----------------------------@.";
  Format.printf "Alice & Bob are revealing their choices...@.";
  let reveal_alice () = call_reveal ~node ~from:alice ~kt1:sos_contract (alice_choice, alice_secret) in
  let reveal_bob () = call_reveal ~node ~from:bob ~kt1:sos_contract (bob_choice, bob_secret) in
  let>? _ = B.parallel_calls (Format.printf "Operation Hash : %s@.") [reveal_alice; reveal_bob] in
  (* End Phase. *)
  Format.printf "------------------------------ EndGame -----------------------------@.";
  Format.printf "Bob ending the game...@.";
  let>? operation_hash = call__end ~node ~from:bob ~kt1:sos_contract () in
  Format.printf "Operation Hash : %s@." operation_hash;
  Format.eprintf "\n\n------------------ End of Split or Steal scenario ------------------\n@.";
  Lwt.return_ok ()

let _ =
  Lwt_main.run (main ())