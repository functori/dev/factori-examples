open Mligo

type state = Registration | Playing | Revealing | EndGame

type storage =
  {
    current_state : state;
    player1 : address option;
    player2 : address option;
    current_players : int;
    choice1_hash : bytes;
    choice2_hash : bytes;
    choice1_confirm : bool;
    choice2_confirm : bool;
    choice1 : string;
    choice2 : string;
  }
[@@comb][@@store]

type answer = Split | Steal | Uknown

let get_answer (s : string) =
  if s = "Split" then Split
  else if s = "Steal" then Steal
  else Uknown


type params =
  | EnterGame
  | Play of bytes
  | Reveal of string * string
  | CreateHash of string * string
  | End
[@@entry Main]

let fail_with (s : string) =
  (failwith s : operation list * storage)

let init_storage : storage =
  {
    current_state = Registration;
    player1 : address option = None;
    player2 : address option = None;
    current_players = 0;
    choice1_hash = Bytes.pack "";
    choice2_hash = Bytes.pack "";
    choice1_confirm = false;
    choice2_confirm = false;
    choice1 = "";
    choice2 = "";
  }
let get_hash ((secret, nonce), _ : (string * string) * storage) : bytes =
  let secret_b, nonce_b = Bytes.pack secret, Bytes.pack nonce in
  let phrase : bytes = Bytes.concat secret_b nonce_b in
  Crypto.sha256 phrase
[@@view]

let get_hash_failwith ((secret, nonce), store : (string * string) * storage) =
  let hashed = get_hash((secret, nonce), store) in
  (failwith hashed : operation list * storage)

let enter_game (_, store : unit * storage) =
  if store.current_state = Registration then
    if Tezos.get_amount None < 210u then
      fail_with "Registration fees has to be greater or equal than 210mutez (0.000210tez)"
    else
      begin
        if store.current_players = 0 then
          ([] : operation list),
          {
            store with
            player1 = Some (Tezos.get_source None);
            current_players = 1
          }
        else if store.current_players = 1 then
          ([] : operation list),
          {
            store with
            player2 = Some (Tezos.get_source None);
            current_players = 0;
            current_state = Playing
          }
        else
          ([] : operation list), store
      end
  else
    fail_with "The game is in REGISTRATION phase."

let play (hashed_answer, store : bytes * storage) =
  if store.current_state = Playing then
    if store.player1 = Some (Tezos.get_source None) then
      if store.choice1_confirm then
        fail_with "You have already played."
      else
        ([] : operation list),
        {
          store with
          choice1_hash = hashed_answer;
          choice1_confirm = true;
          current_players = store.current_players + 1;
          current_state = if store.current_players = 1 then Revealing else Playing
        }
    else if store.player2 = Some (Tezos.get_source None) then
      if store.choice2_confirm then
        fail_with "You have already played."
      else
        ([] : operation list),
        {
          store with
          choice2_hash = hashed_answer;
          choice2_confirm = true;
          current_players = store.current_players + 1;
          current_state = if store.current_players = 1 then Revealing else Playing
        }
    else
      fail_with "You are not registred as a player."
  else
    fail_with "The game is in PLAYING phase."

let reveal ((answer, nonce), store : (string * string) * storage) =
  if store.current_state = Revealing then
    begin
      if store.player1 = Some (Tezos.get_source None) then
        if store.choice1_hash = get_hash((answer, nonce), store) then
          ([] : operation list),
          {
            store with
            choice1 = answer;
            current_state = if store.choice2 = "" then Revealing else EndGame
          }
        else
          fail_with "Your (secret, nonce) do not match the stored hash."
      else if store.player2 = Some (Tezos.get_source None) then
        if store.choice2_hash = get_hash((answer, nonce), store) then
          ([] : operation list),
          {
            store with
            choice2 = answer;
            current_state = if store.choice1 = "" then Revealing else EndGame
          }
        else
          fail_with "Your (secret, nonce) do not match the stored hash."
      else
        fail_with "You are not registred as a player"
    end
  else
    fail_with "The game is in REVEALING phase."

let endGame (_, store : unit * storage) =
  if store.current_state = EndGame then
    let p1 =
      let p1_address = Option.unopt (store.player1) in
      match (Tezos.get_contract_opt None p1_address : (unit, storage) contract option) with
      | None -> failwith "Problem with the address of Player 1"
      | Some(tz1) -> tz1
    in
    let p2 =
      let p2_address = Option.unopt (store.player2) in
      match (Tezos.get_contract_opt None p2_address : (unit, storage) contract option) with
      | None -> failwith "Problem with the address of Player 2"
      | Some(tz1) -> tz1
    in
    let ans1 = get_answer store.choice1 in
    let ans2 = get_answer store.choice2 in
    let store : storage = init_storage in
    match ans1, ans2 with
    | Split, Split ->
      let op1 : operation = Tezos.transaction None unit 200u p1 in
      let op2 : operation = Tezos.transaction None unit 200u p2 in
      ([op1; op2], store : operation list * storage)
    | Split, Steal ->
      let op2 : operation = Tezos.transaction None unit 400u p2 in
      ([op2], store : operation list * storage)
    | Steal, Split ->
      let op1 : operation = Tezos.transaction None unit 400u p1 in
      ([op1], store : operation list * storage)
    | Steal, Steal ->
      ([], store : operation list * storage)
    | Uknown, Split ->
      let op2 : operation = Tezos.transaction None unit 100u p2 in
      ([op2], store : operation list * storage)
    | Uknown, Steal ->
      let op2 : operation = Tezos.transaction None unit 300u p2 in
      ([op2], store : operation list * storage)
    | Split, Uknown ->
      let op1 : operation = Tezos.transaction None unit 100u p1 in
      ([op1], store : operation list * storage)
    | Steal, Uknown ->
      let op1 : operation = Tezos.transaction None unit 300u p1 in
      ([op1], store : operation list * storage)
    | Uknown, Uknown ->
      ([], store : operation list * storage)
  else
    fail_with "The game is in END phase."

let main (action, s: params * storage) : operation list * storage =
  match action with
  | EnterGame -> enter_game ((), s)
  | Play b -> play (b, s)
  | Reveal (a, n) -> reveal((a, n), s)
  | CreateHash (a, n) -> get_hash_failwith ((a, n), s)
  | End -> endGame ((), s)
